package com.example.appcw;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.Vector;

public class SommeilOperation {
    private SommeilDatabase dbHelper;
    private SQLiteDatabase database;


    public SommeilOperation(Context context) {
        dbHelper = new SommeilDatabase(context);
    }


     //Création de la base de données avec un accès en lecture et écriture

    public void open(){
        database = dbHelper.getWritableDatabase();
    }

    //Fermeture de la connexion à la base de données

    public void close() {
        dbHelper.close();


    }
    //Ajout d'une nuit de sommeille à la table "sommeil"
    public long addSommeil(Sommeil s){
        ContentValues valeurs = new ContentValues();
        valeurs.put(dbHelper.getJour(), s.getJour());
        valeurs.put(dbHelper.getHeureLevee(), s.getHeurL());
        valeurs.put(dbHelper.getHeureCouche(), s.getHeurC());

        long SommeilId = database.insert(dbHelper.getTableSommeil(), null, valeurs);
        return SommeilId;
    }

    // Affichage des nuits contenus dans la table "sommeil"

    public Vector<Sommeil> listAllSommeil() {

        String tabColonne[] = new String [4];
        Vector<Sommeil> lSommeil = new Vector<Sommeil>();

        // La requête renvoie l’id, le nom et le numéro de téléphone des
        // contacts.
        tabColonne[0] = dbHelper.getId();
        tabColonne[1] = dbHelper.getJour();
        tabColonne[2] = dbHelper.getHeureLevee();
        tabColonne[3] = dbHelper.getHeureCouche();


        // On exécute la requête. Le résultat est stocké dans la variable
        // cursor.
        Cursor cursor = database.query(dbHelper.getTableSommeil(),
                tabColonne,
                null, null,	null, null,	null);


        // On récupère les valeurs des colonnes jour, heur_levee et heure_couché stockés dans l'objet
        // cursor. On construit un vecteur de "lSommeil"
        int numeroColonneJ =
                cursor.getColumnIndexOrThrow(dbHelper.getJour());
        int numeroColonneHL =
                cursor.getColumnIndexOrThrow(dbHelper.getHeureLevee());
        int numeroColonneHC =
                cursor.getColumnIndexOrThrow(dbHelper.getHeureCouche());
        if (cursor.moveToFirst() == true) {
            do {
                String jour = cursor.getString(numeroColonneJ);
                String numHl =
                        cursor.getString(numeroColonneHL);
                String numHc =
                        cursor.getString(numeroColonneHC);
                Sommeil c = new Sommeil(jour, numHl, numHc);
                lSommeil.add(c);
            } while (cursor.moveToNext());
        }

        return lSommeil;
    }

    // Suppretion des valeur stocker dans la table sommeil
    public void deleteAll(){
        database.execSQL("DELETE FROM " + dbHelper.getTableSommeil());
    }
}

