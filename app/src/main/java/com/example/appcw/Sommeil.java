package com.example.appcw;

public class Sommeil {
    private int id;
    private String jour;
    private String heurL;
    private String  heurC;

    public Sommeil(String jour, String heurL, String heurC){
        this.jour = jour;
        this.heurC = heurC;
        this.heurL = heurL;
    }

    public int getId() {
        return id;
    }


    public String getHeurL() {
        return heurL;
    }

    public String getJour() {
        return jour;
    }

    public String getHeurC() {
        return heurC;
    }
}
