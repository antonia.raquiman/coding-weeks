package com.example.appcw;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
   // Si on clique le bouton Matin, on va
    public void goToMatin(View view) {
        Intent mainToMatin = new Intent();
        mainToMatin.setClass(this, MatinActivity.class);
        startActivity(mainToMatin);
    }

    //Si on clique le bouton Soir, on va
    public void goToSoir(View view) {
        Intent mainToSoir = new Intent();
        mainToSoir.setClass(this, SoirActivity.class);
        startActivity(mainToSoir);

    }

    //Si on clique le bouton Alarme, on va
    public void goToAlarm(View view) {
        Intent mainToAlarm = new Intent();
        mainToAlarm.setClass(this, AlarmActivity.class);
        startActivity(mainToAlarm);

    }

    public void goToBilan(View view) {
        Intent mainToBilan = new Intent();
        mainToBilan.setClass(this, BilanActivity.class);
        startActivity(mainToBilan);

    }
}