package com.example.appcw;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SommeilDatabase extends SQLiteOpenHelper {

    // Version de la base de données
    private static final int VERSION_BASE_DE_DONNEES = 1;
    // Nom du fichier contenant la base de données
    private static final String NOM_BASE_DE_DONNEES = "sommeil.db";
    // Nom de la table qui sera créée dans la base de données
    private static final String TABLE_SOMMEIL = "sommeil";
    // Les champs de la table
    private static final String ID = "id";
    private static final String JOUR = "date";
    private static final String HEURE_LEVEE = "heure_levee";
    private static final String HEURE_COUCHE = "heure_couche";

    // Requête SQL de création de la table "contact" dans la base de données
    private static final String REQUETE_CREATION_TABLE =
            "create table " + TABLE_SOMMEIL +
                    "(" + ID + " integer primary key autoincrement, " +
                    JOUR + " text not null, " +
                    HEURE_LEVEE + " text not null, " + HEURE_COUCHE + " text not null) ;";

    // Requête SQL de suppression de la table "contacts" dans la base de données
    private static final String REQUETE_SUPPRESSION_TABLE =
            "DROP TABLE IF EXISTS " +
                    TABLE_SOMMEIL + ";";


    public String  getJour(){
        return JOUR;
    }

    public String getHeureLevee(){
        return HEURE_LEVEE;
    }
    public String getHeureCouche(){
        return HEURE_COUCHE;
    }

    public String getTableSommeil(){
        return TABLE_SOMMEIL;
    }

    public  String getId(){
        return ID;
    }
    public  int getVersionBaseDeDonnees(){
        return VERSION_BASE_DE_DONNEES;
    }

    public SommeilDatabase(Context context) {
        super(context, NOM_BASE_DE_DONNEES, null, VERSION_BASE_DE_DONNEES);
    }

    //Création de la base de donnée
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(REQUETE_CREATION_TABLE);
    }

   //Met à jour les donnée
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(REQUETE_SUPPRESSION_TABLE);
        onCreate(db);
    }
}

