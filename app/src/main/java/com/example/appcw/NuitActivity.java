package com.example.appcw;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Vector;

public class NuitActivity extends AppCompatActivity {
    private ListView listViewContacts;
    private SommeilOperation  sommeilOperations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuit);

        // On récupère une référence sur la listView
        listViewContacts = (ListView)findViewById(R.id.listview1);

        // On crée l'objet qui permet de manipuler la base de données
        sommeilOperations = new SommeilOperation(this);

        // On stocke dans le vecteur "lSommeil" la liste de contacts
        // contenus dans la table "contacts" de la base de données
        Vector<Sommeil> lSommeil;
        sommeilOperations.open();
        lSommeil = sommeilOperations.listAllSommeil();
        sommeilOperations.close();

        // On associe au modèle de la ListView le vecteur de contacts
        // "lSommeil"
        if (lSommeil != null) {
            String []anArrayString = new String [lSommeil.size()];
            for (int i = 0; i < lSommeil.size(); i++) {
                String s =
                        "Jour : " +lSommeil.get(i).getJour() + " /  S'est couché(e) à " +
                                lSommeil.get(i).getHeurC() + " et s'est levé(e) à " + lSommeil.get(i).getHeurL();
                anArrayString[i] = s;
            }
            ArrayAdapter<String> listArrayAdapter =
                    new ArrayAdapter<String>(this,
                            android.R.layout.simple_list_item_1,
                            anArrayString);
            listViewContacts.setAdapter(listArrayAdapter);
        }
    }
}
