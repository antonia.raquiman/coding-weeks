package com.example.appcw;
import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.timepicker.MaterialTimePicker;
import com.google.android.material.timepicker.TimeFormat;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AlarmActivity extends AppCompatActivity {

    private MaterialTimePicker picker;
    private Calendar c;
    private AlarmManager alarmManager;
    private PendingIntent pendingIntent;
    SimpleDateFormat df = new SimpleDateFormat("HH:mm");
    TextView t;
    Button b1;
    Button b2;
    Button b3;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_alarm);
        createNotificationChanel();

        //Assignation des View
        b1 = findViewById(R.id.selectTime);
        b2 = findViewById(R.id.setTime);
        b3 = findViewById(R.id.cancelAlarm);
        t = findViewById(R.id.textview);


        // Permet de garder l'affichage de l'heure de l'alarme lorsquelon fermet ouvre l'application
        SharedPreferences sp = getSharedPreferences("memNote", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        if (sp.contains(getString(R.string.heur_alarm))){
            t.setText(sp.getString(getString(R.string.heur_alarm), getString(R.string.heur_def_alarme)));
            t.setTextSize(46);
        }

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Méthode pour choisir l'heure
                showTimePicker();

            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Méthode pour mettre en marche l'alamre
                setAlarm();


            }
        });


        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Méthode pour annuler l'alamre
                cancelAlarm();


            }
        });




    }

    private void cancelAlarm() {

        //Relie a AlarmeReceiver pour avoir acces à l'alarme existente
        Intent intent = new Intent(this, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_IMMUTABLE);

        //Si il n'y a pas d'alarme en crée une
        if(alarmManager == null){
            alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);}

        //Annulle l'alarme
        alarmManager.cancel(pendingIntent);
        Toast.makeText(this, R.string.lalarme_est_annulee, Toast.LENGTH_SHORT).show();

    }


    private void setAlarm() {
        String formattedDate = df.format(c.getTime());

        // Crée une alarme et la relie aux infos présente dans AlarmReceiver
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_IMMUTABLE);
        //Programe une alarme qui se repète chaque jours à la date c
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);

        Toast.makeText(this, getString(R.string.lalarme_est_programee_a) + formattedDate, Toast.LENGTH_SHORT).show();
    }

    private void showTimePicker() {
        // Créer l'interface de selection de l'heure
        picker = new MaterialTimePicker.Builder()
                .setTimeFormat(TimeFormat.CLOCK_24H)
                .setHour(12)
                .setMinute(0)
                .setTitleText(R.string.choisir_une_heure)
                .build();
        picker.show(getSupportFragmentManager(), "foxandroid");

        //Met à jour l'affichage de l'heure lorsque celle si est selectionnée  et définie la date c
        picker.addOnPositiveButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp = getSharedPreferences("memNote", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("heur_alarm", picker.getHour() + " : " + String.format("%02d", picker.getMinute()));
                editor.apply();
                t.setText(sp.getString("heur_alarm", "12h30"));
                t.setTextSize(46);
                c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, picker.getHour());
                c.set(Calendar.MINUTE, picker.getMinute());
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);
            }
        });
    }



    private void createNotificationChanel() {

        //Création d'un channel obligatoire pour les version superieur à Android Oreo
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    "foxandroid",
                    "channelAPPCW",
                    NotificationManager.IMPORTANCE_HIGH
            );
            channel.setDescription("channel pour l'applis Appcw");
            NotificationManager manager = this.getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }
    }

}