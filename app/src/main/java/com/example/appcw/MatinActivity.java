package com.example.appcw;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class MatinActivity extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout ll;
    private Date d = new Date();
    private String date = DateFormat.format("MM/dd/yyyy", d.getTime()).toString();
    private EditText etl;
    private TextView tvl;
    private EditText etc;
    private TextView tvc;
    private TextView tex;
    private RatingBar stars;
    private TextView tv;
    private EditText etmed;
    private TextView tv1;
    private EditText et1;
    private TextView tv2;
    private EditText et2;
    private int pass;
    private SommeilOperation sommeilOperations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matin);
        //On apelle la première question
        sommeilOperations = new SommeilOperation(this);
        Question1();

    }
    //Fonction pour écrire sur un fichier
    public void writeToFile(Context context, String FileName, String content) {
        try {
            File path = Environment.getExternalStorageDirectory();
            File file = new File(path, "Documents");
            //Si le fichier n'existe pas on le crée
            if (!file.exists()) {
                file.mkdirs();
            }
            File gpxfile = new File(file, FileName);
            FileWriter writer = new FileWriter(gpxfile, true);


            //On ajoute le contenu
            writer.append(content);
            writer.flush();
            writer.close();
            Toast.makeText(context, getString(R.string.Popup), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void Question1() {
        ll = findViewById(R.id.matin_layout);
        pass = 1;

        // Views à afficher

        tvc = new TextView(this);
        etc = new EditText(this);
        tvl = new TextView(this);
        etl = new EditText(this);
        Button b = new Button(this);

        // Texte à afficher sur les Views
        tvc.setText(getString(R.string.question_heure_de_se_coucher));
        etc.setHint(getString(R.string.exemple_heure_soir));
        tvl.setText(getString(R.string.question_heure_de_se_lever));
        etl.setHint(getString(R.string.exemple_heure_matin));
        b.setText(getString(R.string.Suivant));
        b.setOnClickListener(this);

        // Ajout des Views au Layout
        ll.addView(tvc);
        ll.addView(etc);
        ll.addView(tvl);
        ll.addView(etl);
        ll.addView(b);
    }

    public void Question2() {
        ll = findViewById(R.id.matin_layout);
        pass = 2;
        // Views à afficher
        tex = new TextView(this);
        stars = new RatingBar(this);
        Button bouton = new Button(this);

        // Texte à afficher sur les Views
        tex.setText(getString(R.string.combien_noteriez_vous_votre_nuit));
        bouton.setText(getString(R.string.Suivant));
        bouton.setOnClickListener(this);
        stars.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT));
        stars.setNumStars(5);

        // Ajout des Views au Layout
        ll.addView(tex);
        ll.addView(stars);
        ll.addView(bouton);
    }

    public void Question3() {
        ll = findViewById(R.id.matin_layout);
        pass=3;
        // Views à afficher
        tex = new TextView(this);
        Button bNo = new Button(this);
        Button bYes = new Button(this);

        // Texte à afficher sur les Views
        tex.setText(getString(R.string.question_medicaments));
        bYes.setText(getString(R.string.oui));
        bYes.setOnClickListener(this);
        bNo.setText(getString(R.string.non));
        bNo.setOnClickListener(this);

        // Ajout des Views au Layout
        ll.addView(tex);
        ll.addView(bYes);
        ll.addView(bNo);
    }

    public void medicament() {
        ll = findViewById(R.id.matin_layout);
        pass = 4;

        // Views à afficher
        tv = new TextView(this);
        etmed = new EditText(this);
        Button b = new Button(this);

        // Texte à afficher sur les Views
        tv.setText(getString(R.string.indique_les_medicaments));
        etmed.setHint(getString(R.string.hint_medicaments_pris));
        ll.addView(tv);
        ll.addView(etmed);
        b.setText(getString(R.string.Suivant));
        b.setOnClickListener(this);

        // Ajout des Views au Layout
        ll.addView(b);
    }

    public void question4() {
        ll = findViewById(R.id.matin_layout);
        pass = 5;
        // Views à afficher
        tex = new TextView(this);
        Button bNo = new Button(this);
        Button bYes = new Button(this);

        // Texte à afficher sur les Views
        tex.setText(getString(R.string.votre_nuit_a_t_elle_ete_perturbe));
        bYes.setText(getString(R.string.oui));
        bYes.setOnClickListener(this);
        bNo.setText(getString(R.string.non));
        bNo.setOnClickListener(this);

        // Ajout des Views au Layout
        ll.addView(tex);
        ll.addView(bYes);
        ll.addView(bNo);
    }

    public void perturbation() {
        ll = findViewById(R.id.matin_layout);

        // Views à afficher
        tv1 = new TextView(this);
        et1 = new EditText(this);
        tv2 = new TextView(this);
        et2 = new EditText(this);
        Button b = new Button(this);

        // Texte à afficher sur les Views
        tv1.setText(getString(R.string.duree_du_reveille));
        et1.setHint(getString(R.string.exemple_temps));

        tv2.setText(getString(R.string.raison_du_reveille));
        et2.setHint(getString(R.string.exemple_raison_du_reveille));
        b.setText(getString(R.string.sauvegarder));
        b.setOnClickListener(this);

        // Ajout des Views au Layout
        ll.addView(tv1);
        ll.addView(et1);
        ll.addView(tv2);
        ll.addView(et2);
        ll.addView(b);
    }

    //Onclicks pour chaque bouton
    public void onClick(View v) {
        Button boutonClique = (Button) v;
        if (boutonClique.getText().toString().equals(getString(R.string.Suivant))) {
            if (pass == 1) {
                ll.removeAllViews();
                //On écrit le contenu sur le fichier
                String content = date + "\n" + String.format(getString(R.string.reponse_heure_de_se_coucher), etc.getText().toString()) + "\n";
                writeToFile(this, "Journal.txt", content);
                content = String.format(getString(R.string.reponse_heure_de_se_lever), etl.getText().toString()) + "\n";
                writeToFile(this, "Journal.txt", content);
                sommeilOperations.open();
                sommeilOperations.addSommeil(new Sommeil(date, etl.getText().toString(), etc.getText().toString()));
                sommeilOperations.close();
                //On appelle la question suivante
                Question2();
            }
            else if (pass == 2) {
                ll.removeAllViews();
                //On écrit le contenu sur le fichier
                String content = String.format(getString(R.string.nuit), stars.getRating()) + "\n";
                writeToFile(this, "Journal.txt", content);
                //On appelle la question suivante
                Question3();
            }

            else {
                ll.removeAllViews();
                //On écrit le contenu sur le fichier
                String content =String.format(getString(R.string.jai_pris_des_medicaments),etmed.getText().toString())+"\n";
                writeToFile(this, "Journal.txt", content);
                //On appelle la question suivante
                question4();
            }
        }
        else if (boutonClique.getText().toString().equals(getString(R.string.non))){
            if (pass == 3) {
                ll.removeAllViews();
                //On écrit le contenu sur le fichier
                String content = getString(R.string.je_nai_pas_pris_des_medicaments) + "\n";
                writeToFile(this, "Journal.txt", content);
                //On appelle la question suivante
                question4();
            }
            else {
                //On écrit le contenu sur le fichier
                String content =getString(R.string.nuit_pas_perturbee)+"\n";
                writeToFile(this, "Journal.txt", content);
                //On revient sur la MainActivity
                Intent backToMain = new Intent();
                backToMain.setClass(this, MainActivity.class);
                startActivity(backToMain);

            }
        }

        else if (boutonClique.getText().toString().equals(getString(R.string.oui))){
            if (pass == 3) {
                ll.removeAllViews();
                medicament();
            }
            else if (pass == 5){
                ll.removeAllViews();
                perturbation();
            }
        }

        else if (boutonClique.getText().toString().equals(getString(R.string.sauvegarder))){
            String content =String.format(getString(R.string.reponse_duree_du_reveille),et1.getText().toString(),et2.getText().toString())+"\n";
            writeToFile(this, "Journal.txt", content );
            Intent backToMain = new Intent();
            backToMain.setClass(this, MainActivity.class);
            startActivity(backToMain);

        }

    }
}






