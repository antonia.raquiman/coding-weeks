package com.example.appcw;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class SoirActivity extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout ll;
    private TextView question1;
    private TextView question2;
    private TextView question3;
    private EditText response1;
    private EditText response2;
    private int pass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soir);
        QuestionSieste();
    }
    //Fonction pour écrire sur un fichier
    public void writeToFile(Context context, String FileName , String content){
        try{
            File path = Environment.getExternalStorageDirectory();
            File file = new File(path, "Documents");
            //Si le fichier n'existe pas on le crée
            if (!file.exists())
            {
                file.mkdirs();
            }
            File gpxfile = new File(file, FileName);
            FileWriter writer = new FileWriter(gpxfile,true);
            //On ajoute le contenu
            writer.append(content);
            writer.flush();
            writer.close();
            //On crée un PopUp pour savoir que c'est écrit
            Toast.makeText(context,
                    getString(R.string.ecrit_sur_le_fichier),
                    Toast.LENGTH_SHORT).show();
        }

        catch (IOException e) {
            e.printStackTrace();
        }
    }

    //On crée la premiére question
    public void QuestionSieste() {
        pass = 1;
        ll = findViewById(R.id.SoirLayout);
        question1 = new TextView(this);
        question1.setText(getString(R.string.question_sieste));
        ll.addView(question1);
        Button oui = new Button(this);
        oui.setText(getString(R.string.oui));
        oui.setOnClickListener(this);
        ll.addView(oui);
        Button non = new Button(this);
        non.setText(getString(R.string.non));
        non.setOnClickListener(this);
        ll.addView(non);
    }

// On crée la deuxième question
    public void QuestionSomnolence() {
        pass = 2;
        ll = findViewById(R.id.SoirLayout);
        question2 = new TextView(this);
        question2.setText(getString(R.string.question_moments_de_somnolence));
        ll.addView(question2);
        Button oui = new Button(this);
        oui.setText(getString(R.string.oui));
        oui.setOnClickListener(this);
        ll.addView(oui);
        Button non = new Button(this);
        non.setText(getString(R.string.non));
        non.setOnClickListener(this);
        ll.addView(non);
    }
// On crée les question sur les horaires
    public void Horaires() {
        ll = findViewById(R.id.SoirLayout);
        question3 = new TextView(this);
        question3.setText(getString(R.string.a_quelle_heure));
        ll.addView(question3);
        response1 = new EditText(this);
        response1.setHint(getString(R.string.exemple_heure_soir));
        ll.addView(response1);
        question3 = new TextView(this);
        question3.setText(getString(R.string.pour_combien_de_temps));
        ll.addView(question3);
        response2 = new EditText(this);
        response2.setHint(getString(R.string.exemple_temps));
        ll.addView(response2);
        Button ok = new Button(this);
        ok.setText(getString(R.string.ok));
        ok.setOnClickListener(this);
        ll.addView(ok);
    }

//On define les onClick pour chaque bouton
    @Override
    public void onClick(View v) {
        Button boutonClique = (Button) v;
        //On défine l'onClick pour le Oui
        if (boutonClique.getText().toString().equals(getString(R.string.oui))) {
            ll.removeAllViews();
            //On pass à la question suivante
            Horaires();
        }
        //On défine l'onClick pour le Non
        if (boutonClique.getText().toString().equals(getString(R.string.non))) {
            if (pass == 1) {
                ll.removeAllViews();
                String content = getString(R.string.je_nai_pas_fait_une_sieste)+"\n";
                //On écrit les info sur le fichier
                writeToFile(this, "Journal.txt", content);
                //On pass à la question suivante
                QuestionSomnolence();
            }
            else {
                String content = getString(R.string.je_nai_pas_eu_moments_de_somnolence)+"\n";
                //On écrit les infos sur le fichier
                writeToFile(this, "Journal.txt", content);
                //On revient à la MainActivity
                Intent backToMain = new Intent();
                backToMain.setClass(this, MainActivity.class);
                startActivity(backToMain);
                finish();
            }
        }
        //On défine l'onClick pour le Ok
        if (boutonClique.getText().toString().equals(getString(R.string.ok))) {
            if (pass == 1) {
                ll.removeAllViews();
                String content = String.format(getString(R.string.jai_fait_une_sieste),response1.getText().toString(),response2.getText().toString())+"\n";
                //On écrit les infos sur le fichier
                writeToFile(this, "Journal.txt", content);
                QuestionSomnolence();
            }
            else {
                String content = String.format(getString(R.string.jai_eu_un_moment_de_somnolence),response1.getText().toString(),response2.getText().toString()+"\n");
                //On écrit les infos sur le fichier
                writeToFile(this, "Journal.txt",content);
                //On revient à la MainActivity
                Intent backToMain = new Intent();
                backToMain.setClass(this, MainActivity.class);
                startActivity(backToMain);
                finish();
            }
        }

    }

}


