package com.example.appcw;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.view.View;

import java.io.File;

public class BilanActivity extends AppCompatActivity {
    private SommeilOperation  sommeilOperations;
    private File fileLocation= new File(Environment.getExternalStorageDirectory(),"Documents");
    private File FileLocation = new File(fileLocation,"Journal.txt");
    private Intent send;
    private Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bilan);

        ActivityCompat.requestPermissions(this,new String[]{READ_EXTERNAL_STORAGE,WRITE_EXTERNAL_STORAGE}, PackageManager.PERMISSION_GRANTED);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }


    public void sendEmail(View view){

        uri = Uri.parse(String.valueOf(FileLocation));
        send = new Intent(Intent.ACTION_SEND);
        // On défine le type comme 'email'
        send.setType("message/rfc822");
        // Le attachment
        send.putExtra(Intent.EXTRA_STREAM,uri);
        // Le sujet du mail
        send.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.subject_email));
        startActivity(Intent.createChooser(send , getString(R.string.email_chooser)));
    }


    public void goToSommeil(View view) {
        Intent i = new Intent();
        i.setClass(this, NuitActivity.class);
        startActivity(i);

    }

    public void reinitialiser(View view) {
        sommeilOperations = new SommeilOperation(this);
        sommeilOperations.open();
        sommeilOperations.deleteAll();
        sommeilOperations.close();
    }
}